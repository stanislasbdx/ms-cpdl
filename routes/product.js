module.exports = function (api, opts, done) {
	api.get("/:productId/state", async (request, reply) => {
		return {
			product: request.params.productId,
			state: 1
		};
	});

	api.post("/:productId/state", {
		schema: {
			body: {
				newState: { type: "integer" },
			}
		},
		handler(request, reply) {
			reply.send({
				product: request.params.productId,
				newState: request.body.state,
			});
		},
	});

	done();
};