const api = require("fastify")({ logger: true });

api.register(require("./routes/product"), { prefix: "/product" });

api.get("/ping", async (request, reply) => {
	return "Pong!";
});

const start = async () => {
	try {
		await api.listen({ port: 3000 });
	} catch (err) {
		api.log.error(err);
		process.exit(1);
	}
};

start();
