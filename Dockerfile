FROM node:current-alpine

# Distrib upgrades
RUN apk update
RUN apk upgrade
RUN apk add curl bash

# NPM configuration
RUN apk add npm
RUN npm install -g npm@latest node-prune

## Install node-prune (https://github.com/tj/node-prune)
RUN curl -sfL https://install.goreleaser.com/github.com/tj/node-prune.sh | bash -s -- -b /usr/local/bin

# Copy app files
WORKDIR /app

COPY package*.json ./

COPY . .

# App install
RUN npm install --omit=dev

# Clean image
RUN node-prune
RUN npm prune

# Run app
EXPOSE 3000
CMD ["npm", "run", "serve"]