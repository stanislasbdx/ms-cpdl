# 🌀 MS Lifecycle CPDL

## Small product lifecycle API

We recommand the usage of [Yarn](https://yarnpkg.com) instead of [NPM](https://www.npmjs.com).

### Installation

```pwsh
$ yarn install
yarn install v1.22.19
[1/4] Resolving packages...
[2/4] Fetching packages...
[3/4] Linking dependencies...
[4/4] Building fresh packages...
success Saved lockfile.
Done in 11.46s.
```

### Usage

A swagger is available in the repository : <https://gitlab.com/stanislasbdx/ms-cpdl/-/blob/main/swagger.json>

> By default, the API will listen the 3000 port.

#### With CLI

```pwsh
# Start the server
$ yarn serve
{"level":30,"time":1684222189067,"pid":6808,"hostname":"xxx","msg":"Server listening at http://127.0.0.1:3000"}
{"level":30,"time":1684222189071,"pid":6808,"hostname":"xxx","msg":"Server listening at http://[::1]:3000"}

# Start the server in dev mode (it'll refresh each time the code files is modified)
$ yarn dev
[nodemon] 2.0.22
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node index.js`
{"level":30,"time":1684222189067,"pid":6808,"hostname":"xxx","msg":"Server listening at http://127.0.0.1:3000"}
{"level":30,"time":1684222189071,"pid":6808,"hostname":"xxx","msg":"Server listening at http://[::1]:3000"}
```

#### With Docker

You can pull a specific version of the API by replacing *latest* with the [repository tags](https://gitlab.com/stanislasbdx/ms-cpdl/-/tags).

```pwsh
$ docker run registry.gitlab.com/stanislasbdx/ms-cpdl/main:latest
latest: Pulling from stanislasbdx/ms-cpdl/main
...
56618218a026ac388bcda3aeb30977ef671d593ddfc7dacc8a828131f14f953b
```
